# Python_structure_template

> Template of repo structure for python projects

This repository contains the initial structure to start a python project.

![](logo_python.png)

After spending a lot of time looking for the ideal python project tree, I decided to make a template from my previous projects. This work is largely based on the template already created by [navdeep-G](https://github.com/navdeep-G) that I slightly modified to fit my needs. For more information about the perfect structure you can read [this article](https://docs.python-guide.org/writing/structure/).

## Usage

1. Get the tree structure
    ```sh
    git clone --depth 1 https://gitlab.com/A1CY0N/python_boilerplate.git
    ```
2. Go to the directory
    ```sh
    cd python_boilerplate
    ```
3. Change the repository origin with the new repository URL for your project
    ```sh
    git remote add origin git@gitlab.com/[username]/[project_name].git
    ```
4. Verify the new remote URL
    ```sh
    git remote -v
    ```
4. Push the changes in your local repository to the master branch of your remote repository (Gitlab or Github)
    ```sh
    git push origin master
    ```

## Meta

[@A1CY0N](https://mamot.fr/@a1c0n) – a1cy0n@tutanota.com

[@Navdeep-G](https://github.com/navdeep-G) – Navdeep Gill

Distributed under the MIT license. See ``LICENSE`` for more information.

[https://gitlab.com/A1CY0N](https://gitlab.com/A1CY0N)

## Contributing

1. Fork it (<https://gitlab.com/A1CY0N/python_boilerplate>)
2. Create your feature branch (`git checkout -b feature/fooBar`)
3. Commit your changes (`git commit -am 'Add some fooBar'`)
4. Push to the branch (`git push origin feature/fooBar`)
5. Create a new Pull Request